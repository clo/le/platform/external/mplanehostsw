Name:           libnetconf2
Version:        %{version}
Release:        1%{?dist}
License:        BSD-3-Clause
Summary:        libnetconf2
BuildArch:      x86_64

BuildRequires:  cmake libssh-devel

%global __brp_ldconfig %{nil}

%description
libnetconf2 for mplane netconf-client.

%prep
git clone -q --single-branch --branch v1.1.36 https://git.codelinaro.org/clo/le/libnetconf2  > /dev/null 2>&1

%build
cmake --log-level=ERROR -B build -DCMAKE_INSTALL_PREFIX:PATH=%{_prefix} -DENABLE_BUILD_TESTS=OFF -DENABLE_VALGRIND_TESTS=OFF libnetconf2
cd build
make -j

%install
cd build
make DESTDIR=%{buildroot} install

%clean
if [ "$RPM_BUILD_ROOT" != "/" ]; then
        rm -rf $RPM_BUILD_ROOT
fi

%files
%defattr(-,root,root)
/usr/lib64/libnetconf2.so*
/usr/lib64/pkgconfig/libnetconf2.pc
/usr/include/libnetconf2/*
/usr/include/nc_client.h
/usr/include/nc_server.h
%dir /usr/include/libnetconf2/
