Name:           mplanehostsw_oss
Version:        %{version}
Release:        1%{?dist}
License:        BSD-3-Clause & MIT
Summary:        RHEL pkg for mplanehostsw_oss
BuildArch:      x86_64

%global __brp_ldconfig %{nil}

%description
Modules for Mplane Host SW

%prep
cd %{_sourcedir}/
rm -rf oss
mkdir oss
cd oss

git clone -q --single-branch --branch caf3/libyang1 https://git.codelinaro.org/clo/le/libyang.git
cd libyang
git reset --hard 127c1a97784f0dacc44b93f7f19d57ffcd5e8de4
mkdir build
cd build
cmake -DCMAKE_LIBRARY_PATH=$(pwd)/install/lib -DCMAKE_MODULE_PATH=%{_sourcedir}/oss/libyang/ -DENABLE_BUILD_TESTS=FALSE -DENABLE_BUILD_FUZZ_TARGETS=FALSE -DGEN_LANGUAGE_BINDINGS=TRUE -DGEN_CPP_BINDINGS=TRUE -DGEN_PYTHON_BINDINGS=FALSE ..
make -k
make install

cd ../../

%install

if [ "$RPM_BUILD_ROOT" != "/" ]; then
        rm -rf $RPM_BUILD_ROOT
fi

mkdir -p $RPM_BUILD_ROOT%{_usr}/local/bin
mkdir -p $RPM_BUILD_ROOT%{_usr}/local/lib64/libyang1/extensions/
mkdir -p $RPM_BUILD_ROOT%{_usr}/local/lib64/libyang1/user_types/
mkdir -p $RPM_BUILD_ROOT%{_usr}/local/lib64/pkgconfig/
mkdir -p $RPM_BUILD_ROOT/usr/share/cmake/Modules/
mkdir -p $RPM_BUILD_ROOT%{_usr}/local/include/libyang/
mkdir -p $RPM_BUILD_ROOT/usr/share/man/man1/

cp -a /usr/local/lib64/libyang.so.1.10.42  $RPM_BUILD_ROOT%{_usr}/local/lib64/
cp -a /usr/local/lib64/libyang.so.1  $RPM_BUILD_ROOT%{_usr}/local/lib64/
cp -a /usr/local/lib64/libyang.so  $RPM_BUILD_ROOT%{_usr}/local/lib64/

cp -a /usr/local/lib64/libyang-cpp.so.1.10.42  $RPM_BUILD_ROOT%{_usr}/local/lib64/
cp -a /usr/local/lib64/libyang-cpp.so.1  $RPM_BUILD_ROOT%{_usr}/local/lib64/
cp -a /usr/local/lib64/libyang-cpp.so  $RPM_BUILD_ROOT%{_usr}/local/lib64/

cp -a /usr/local/lib64/pkgconfig/libyang.pc $RPM_BUILD_ROOT%{_usr}/local/lib64/pkgconfig/
cp -a /usr/local/lib64/pkgconfig/libyang-cpp.pc $RPM_BUILD_ROOT%{_usr}/local/lib64/pkgconfig/

cp -a /usr/local/lib64/libyang1/* $RPM_BUILD_ROOT%{_usr}/local/lib64/libyang1/

cp -a /usr/local/include/libyang/* $RPM_BUILD_ROOT%{_usr}/local/include/libyang/

cp -a /usr/local/share/man/man1/yanglint.1  $RPM_BUILD_ROOT/usr/share/man/man1/
cp -a /usr/local/share/man/man1/yangre.1  $RPM_BUILD_ROOT/usr/share/man/man1/


cp %{_sourcedir}/oss/libyang/FindLibYANG.cmake $RPM_BUILD_ROOT/usr/share/cmake/Modules/

cp -a /usr/local/bin/yanglint $RPM_BUILD_ROOT%{_usr}/local/bin/
cp -a /usr/local/bin/yangre $RPM_BUILD_ROOT%{_usr}/local/bin/

%clean
if [ "$RPM_BUILD_ROOT" != "/" ]; then
        rm -rf $RPM_BUILD_ROOT
        rm -rf /usr/local/lib64/libyang*
        rm -rf /usr/local/lib64/pkgconfig/libyang*
        rm -rf /usr/local/include/libyang*
        rm -rf /usr/local/bin/yanglint
        rm -rf /usr/local/bin/yangre
        rm -rf /usr/local/share/man/man1/yanglint.1
        rm -rf /usr/local/share/man/man1/yangre.1
fi

%files
%defattr(-,root,root)
/usr/local/lib64/libyang1/
/usr/local/bin/yanglint
/usr/local/bin/yangre
/usr/local/include/libyang/
/usr/local/lib64/libyang-cpp.so
/usr/local/lib64/libyang-cpp.so.1
/usr/local/lib64/libyang-cpp.so.1.10.42
/usr/local/lib64/libyang.so
/usr/local/lib64/libyang.so.1
/usr/local/lib64/libyang.so.1.10.42
/usr/local/lib64/pkgconfig/libyang-cpp.pc
/usr/local/lib64/pkgconfig/libyang.pc
/usr/share/cmake/Modules/FindLibYANG.cmake
/usr/share/man/man1/yanglint.1.gz
/usr/share/man/man1/yangre.1.gz

%changelog
* %(date "+%a %b %d %Y") %packager %{version}-%{release}
- Initial build of Proto OAM related OSS modules

