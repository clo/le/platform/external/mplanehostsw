Name:           libconfig
Version:        %{version}
Release:        1%{?dist}
License:        GNU LGPL v2.1
Summary:        RHEL pkg for libconfig
BuildArch:      x86_64

%description
libconfig-library for Mplane Host SW

%prep
cd %{_sourcedir}/
rm -rf libconfig-src
mkdir libconfig-src
cd libconfig-src
wget https://hyperrealm.github.io/libconfig/dist/libconfig-1.7.2.tar.gz
tar -xf libconfig-1.7.2.tar.gz

%build
cd %{_sourcedir}/libconfig-src/libconfig-1.7.2
%configure
make -j

%install
if [ "$RPM_BUILD_ROOT" != "/" ]; then
        rm -rf $RPM_BUILD_ROOT
fi

cd %{_sourcedir}/libconfig-src/libconfig-1.7.2
make install DESTDIR=$RPM_BUILD_ROOT
# Remove unused static libraries and info files
rm $RPM_BUILD_ROOT/usr/lib64/libconfig*a
rm $RPM_BUILD_ROOT/usr/share/info/libconfig.info
rm $RPM_BUILD_ROOT/usr/share/info/dir

%files
%{_includedir}
%{_libdir}/pkgconfig
%{_libdir}/cmake/libconfig
%{_libdir}/cmake/libconfig++
%{_libdir}/libconfig.so*
%{_libdir}/libconfig++.so*
